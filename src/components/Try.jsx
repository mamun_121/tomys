import React, { Component } from 'react';

class Try extends Component{
  render() {
    return (
        <div>
          <section className="colorlib-work" data-section="projects">
            <div className="colorlib-narrow-content">
              <div className="row">
                <div className="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
                  <span className="heading-meta">My Work</span>
                  <h2 className="colorlib-heading animate-box">Recent Projects</h2>
                </div>
              </div>
            </div>

            <div className="container">
              <div className="row text-center">
                <div className="col-sm-3 col-lg-offset-1 animate-box" data-animate-effect="fadeInLeft">
                  <div className="card">
                    <div className="card__side card__side--front">
                      <div className="card__picture card__picture--1">
                        &nbsp;
                      </div>
                      <h4 className="card__heading">
                        <span className="card__heading-span card__heading-span--1">The Sea Explorer</span>
                      </h4>
                      <div className="card__details">
                        <p className='text-justify'>
                          Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, ipsum sapiente aspernatur libero repellat quis consequatur
                          ducimus quam nisi exercitationem omnis earum qui. Aperiam, ipsum sapiente aspernatur libero.
                        </p>
                      </div>
                    </div>
                    <div className="card__side card__side--back card__side--back-1">
                      <div className="card__cta">
                        <a href="#popup" className="btn btn--white">Github Link</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-3 offset-1 animate-box" data-animate-effect="fadeInRight">
                  <div className="card">
                    <div className="card__side card__side--front">
                      <div className="card__picture card__picture--2">
                        &nbsp;
                      </div>
                      <h4 className="card__heading">
                        <span className="card__heading-span card__heading-span--2">The Forest Hiker</span>
                      </h4>
                      <div className="card__details">
                        <p className="text-justify">
                          Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, ipsum sapiente aspernatur libero repellat quis consequatur
                          ducimus quam nisi exercitationem omnis earum qui. Aperiam, ipsum sapiente aspernatur libero.
                        </p>
                      </div>

                    </div>
                    <div className="card__side card__side--back card__side--back-2">
                      <div className="card__cta">
                        <a href="#popup" className="btn btn--white">Github Link</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-3 animate-box" data-animate-effect="fadeInTop">
                  <div className="card">
                    <div className="card__side card__side--front">
                      <div className="card__picture card__picture--3">
                        &nbsp;
                      </div>
                      <h4 className="card__heading">
                        <span className="card__heading-span card__heading-span--3">The Snow Adventurer</span>
                      </h4>
                      <div className="card__details">
                        <p className="text-justify">
                          Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, ipsum sapiente aspernatur libero repellat quis consequatur
                          ducimus quam nisi exercitationem omnis earum qui. Aperiam, ipsum sapiente aspernatur libero.
                        </p>
                      </div>

                    </div>
                    <div className="card__side card__side--back card__side--back-3">
                      <div className="card__cta">
                        <a href="#popup" className="btn btn--white">Github Link</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </section>
        </div>
    );
  }
}

export default Try;