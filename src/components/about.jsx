import React, { Component } from 'react'

export default class About extends Component {
  render() {
    return (
      <div>
        <section className="colorlib-about" data-section="about">
        <div className="colorlib-narrow-content">
            <div className="row">
            <div className="col-md-12">
                <div className="row row-bottom-padded-sm animate-box" data-animate-effect="fadeInLeft">
                <div className="col-md-12">
                    <div className="about-desc">
                    <span className="heading-meta">About Us</span>
                    <h2 className="colorlib-heading">Who Am I?</h2>
                    <p className='text-justify'>
                        My name is Mamunur Rashid. I am a student of Fachhochschule Kiel, information Engineering (MSc).
                        Currently, I am doing my master's thesis with the help of Kartonar.de.
                        My thesis topic: An efficient scalable IT-assisted purchasing system for home utilities in the one-stop center.
                        Before starting the master's thesis, I worked here as an intern for seven months.
                        Usually, I work here as a frontend developer, sometimes in the backend.
                        Before came to Germany, I was a lecturer at a private university in Bangladesh.
                        I have completed my Bachelor of Science in Electronics and Telecommunication Engineering in 2014.
                    </p>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </section>
      </div>
    )
  }
}
