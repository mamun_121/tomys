import React, { Component } from 'react'

export default class Sidebar extends Component {
  render() {
    return (
      <div>
        <div>
          <nav href="#navbar" className="js-colorlib-nav-toggle colorlib-nav-toggle" data-toggle="collapse" data-target="#navbar"
               aria-expanded="true" aria-controls="navbar"><i /></nav>
          <aside id="colorlib-aside" className="border js-fullheight">
            <div className="text-center">
              <div className="author-img" style={{backgroundImage: 'url(images/pro.jpg)'}} />
              <h1 id="colorlib-logo"><a href="index.html">Mamunur Rashid</a></h1>
              <span className="email" style={{color: "#a84db8", fontSize:15}}>
                <i className="icon-mail" style={{fontSize:'medium', fontWeight: 500}}></i>
                <a href='mailto:mamun.ruet.ete@gmail.com' style={{color: "#a84db8", fontSize:18, textAlign:'justify'}}>mamun.ruet.ete@gmail.com</a>
              </span>
            </div>
            <nav id="colorlib-main-menu" role="navigation" className="navbar">
              <div id="navbar" className="collapse">
                <ul>
                  <li className="active"><a href="#home" data-nav-section="home">Introduction</a></li>
                  <li><a href="#about" data-nav-section="about">About</a></li>
                  <li><a href="#projects" data-nav-section="projects">Projects</a></li>
                  <li><a href="#experience" data-nav-section="experience">Experience</a></li>
                  <li><a href="#timeline" data-nav-section="timeline">Timeline</a></li>
                </ul>
              </div>
            </nav>
            <nav id="colorlib-main-menu">
              <ul>
                <li><a href="https://www.linkedin.com/in/md-mamunur-rashid-36a222121/" target="_blank" rel="noopener noreferrer"><i className="icon-linkedin2" /></a></li>
                <li><a href="https://www.xing.com/profile/MdMamunur_Rashid4/cv" target="_blank" rel="noopener noreferrer"><i className="icon-xing" /></a></li>
                <li><a href="https://github.com/mamunur121" target="_blank" rel="noopener noreferrer"><i className="icon-github"></i></a></li>
                <li><a href="https://www.facebook.com/mamunur.rashid.357" target="_blank" rel="noopener noreferrer"><i className="icon-facebook2" /></a></li>
                {/* <li><a href="https://twitter.com/ddbarochiya" target="_blank" rel="noopener noreferrer"><i className="icon-twitter2" /></a></li>*/}
                <li><a href="https://www.instagram.com/mamunur_rashid_121/" target="_blank" rel="noopener noreferrer"><i className="icon-instagram" /></a></li>
                <li><a href="https://medium.com/@mamun.ruet.ete" target="_blank" rel="noopener noreferrer"><i className="icon-blogger2"></i></a></li>
              </ul>
            </nav>

            {
              /*
              <div className="colorlib-footer">
              <p><small>
                  Made with <i className="icon-heart-outline" aria-hidden="true" /> and <i className="icon-coffee" aria-hidden="true"></i><br></br>
                  Thanks <a href="https://www.kartonara.de/" target="_blank" rel="noopener noreferrer">Kartonara.de</a> for inspiration
              </small></p>
              <p><small>
                Something coming soon !!
              </small></p>
            </div>

               */
            }

          </aside>
        </div>
      </div>
    )
  }
}
