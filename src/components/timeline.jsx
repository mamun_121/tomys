import React, { Component } from 'react'

export default class Timeline extends Component {
  render() {
    return (
      <div>
        <section className="colorlib-experience" data-section="timeline">
          <div className="colorlib-narrow-content">
            <div className="row">
              <div className="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
                <span className="heading-meta">highlights</span>
                <h2 className="colorlib-heading animate-box">Timeline</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className="timeline-centered">
                  <article className="timeline-entry animate-box" data-animate-effect="fadeInLeft">
                    <div className="timeline-entry-inner">
                      <div className="timeline-icon color-3">
                        <i className="icon-pen2" />
                      </div>
                      <div className="timeline-label">
                        <h2>Master Thesis at <a href='https://www.kartonara.de/' target="_blank" rel="noopener noreferrer">kartonara.de</a>
                          <span> February 2020 to --</span></h2>
                        <p><b>Topic: </b> An efficient scalable IT-assisted purchasing system for home utilities in the one-stop center. <br/>
                          Based on the purchasing history from Cartonara.de, If we think a user (maybe all users)
                          need new gas, electricity and DSL services, we will send a welcome message.
                          I will implement gas, electricity, and DSL service functionality for both web and mobile versions in my master thesis.
                        </p>
                      </div>
                    </div>
                  </article>
                  <article className="timeline-entry animate-box" data-animate-effect="fadeInTop">
                    <div className="timeline-entry-inner">
                      <div className="timeline-icon color-4">
                        <i className="icon-pen2" />
                      </div>
                      <div className="timeline-label">
                        <h2>Web Developer at <a href='https://www.kartonara.de/' target="_blank" rel="noopener noreferrer">kartonara.de</a>
                           <span> July 2019-January 2020</span></h2>
                        <p>• Front-end Development with React.js, JavaScript, HTML, CSS, Bootstrap, Semantic UI
                          <br/>
                          • Backend development with Express.js, MongoDB
                        </p>
                      </div>
                    </div>
                  </article>
                  <article className="timeline-entry animate-box" data-animate-effect="fadeInTop">
                    <div className="timeline-entry-inner">
                      <div className="timeline-icon color-5">
                        <i className="icon-pen2" />
                      </div>
                      <div className="timeline-label">
                        <h2>Web Developer at <a href='https://www.snipclip.com/' target="_blank" rel="noopener noreferrer">SnipClip.com</a>
                          <span> July 2018-December 2018</span>
                        </h2>
                        <p>• Development of Responsive Websites. <br/>
                          • Front-end Development for Web-Based Applications. <br/>
                          • Development and Maintenance of Existing System.</p>
                      </div>
                    </div>
                  </article>
                  <article className="timeline-entry animate-box" data-animate-effect="fadeInLeft">
                    <div className="timeline-entry-inner">
                      <div className="timeline-icon color-6">
                        <i className="icon-pen2" />
                      </div>
                      <div className="timeline-label">
                        <h2>Lecturer at <a href='http://www.bu.edu.bd/' target="_blank" rel="noopener noreferrer">Bangladesh University</a>
                          <span> February 2015-March 2016</span>
                        </h2>
                        <p><b>Taught Courses: </b>Object-Oriented Programming Using C++, Software Engineering and System
                          Analysis and Design, Web Development</p>
                      </div>
                    </div>
                  </article>
                  <article className="timeline-entry animate-box" data-animate-effect="fadeInTop">
                    <div className="timeline-entry-inner">
                      <div className="timeline-icon color-7">
                        <i className="icon-pen2" />
                      </div>
                      <div className="timeline-label">
                        <h2>Bachelor of Science in Electronics and Telecommunication Engineering at <br />
                          <a href='http://www.ruet.ac.bd/' target="_blank" rel="noopener noreferrer">
                            Rajshahi University of Engineering and Technology (RUET), Bangladesh
                          </a>
                          <span> 2019-2014</span>
                        </h2>
                        <p><b>Bachelor Thesis:</b> Reducing Peak to Average Power Ratio (PAPR) using selected
                          mapping technique of OFDM system</p>
                      </div>
                    </div>
                  </article>
                  <article className="timeline-entry begin animate-box" data-animate-effect="fadeInBottom">
                    <div className="timeline-entry-inner">
                      <div className="timeline-icon color-none">
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}
